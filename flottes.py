#! /usr/bin/env python3
# coding: UTF-8
"""
Script: flottes.py
"""
import os
import pickle
import outils
import reseau
import string
import random

# Fonctions à développer selon le cahier des charges
def initialisation_flotte_par_dictionnaire_fixe(nom):
    """renvoie une flotte en dict"""

    if nom == "flotte1":
        flotte1 = {'bateaux': {'porte-avions': ["A1", "B1", "C1", "D1", "E1"],
                               'croiseur': ["A3", "A4", "A5", "A6"],
                               'contre-torpilleurs': ["J8", "J9", "J10"],
                               'sous-marin': ["F1", "F2", "F3"],
                               'torpilleur': ["D2", "E2"]},
                   'tirs': [],
                   'effets': [],
                   'pseudo': 'moi',
                   'nbreTouche': 0,
                   'nbreCoule': 0}

        return flotte1

    elif nom == "flotte2":
        flotte2 = {'bateaux': {'porte-avions': ["A1", "A2", "A3", "A4", "A5"],
                               'croiseur': ["B1", "B2", "B3", "B4"],
                               'contre-torpilleurs': ["D1", "D2", "D3"],
                               'sous-marin': ["E1", "E2", "E3"],
                               'torpilleur': ["H3", "H4"]},
                   'tirs': [],
                   'effets': [],
                   'pseudo': 'adversaire',
                   'nbreTouche': 0,
                   'nbreCoule': 0}
        return flotte2
    elif nom == "flotte_bateau_touche":
        flotte_bateau_touche = {'bateaux': {'porte-avions': ["A1", "B1", "C1", "D1", "E1"],
                                            'croiseur': ["A3", "A4", "A5", "A6"],
                                            'contre-torpilleurs': ["J8", "J9", "J10"],
                                            'sous-marin': ["F1", "F2", "F3"],
                                            'torpilleur': ["D2", "E2"]},
                                'tirs': ["A3", "A4", "A5"],
                                'effets': ["touche", "touche", "touche"],
                                'pseudo': 'moi',
                                'nbreTouche': 3,
                                'nbreCoule': 0}
        return flotte_bateau_touche
    elif nom == "flotte_presque_coulee":
        flotte_presque_coulee = {'bateaux': {'porte-avions': ["A1", "A2", "A3", "A4", "A5"],
                                             'croiseur': ["B1", "B2", "B3", "B4"],
                                             'contre-torpilleurs': ["D1", "D2", "D3"],
                                             'sous-marin': ["E1", "E2", "E3"],
                                             'torpilleur': ["H3", "H4"]},
                                 'tirs': ["A1", "A2", "A3", "A4", "A5",
                                          "B1", "B2", "B3", "B4",
                                          "D1", "D2", "D3",
                                          "E1", "E2", "E3",
                                          "H3"],
                                 'effets': ["touche", "touche", "touche", "touche", "coule",
                                            "touche", "touche", "touche", "coule",
                                            "touche", "touche", "coule",
                                            "touche", "touche", "coule",
                                            "touche"],
                                 'pseudo': 'moi',
                                 'nbreTouche': 11,
                                 'nbreCoule': 5}
        return flotte_presque_coulee


def positions_bateaux(flotte, excepte=""):
    """renvoie un dictionnaire reliant les cases Ln au bateaux de la flotte
    et permet d'enlever 1 bateau s'il est détruit par exemple"""
    bateau = flotte["bateaux"]
    inverse = {}
    for cles in bateau:
        if cles != excepte:
            for valeur in bateau[cles]:
                    inverse[valeur] = cles
    return inverse


def est_bateau_coule(flotte, bateau):
    """Renvoie true si le bateau est coulé ou false si il est pas coulée"""

    tirs = flotte["tirs"]
    bateaux = flotte["bateaux"]
    bateau_voulu = bateaux[bateau]
    nb = []
    for valeur in bateau_voulu:
        for tir in tirs:
            if tir in valeur:
                nb.append("touche")
    if len(nb) == len(bateau_voulu):
        return True
    else:
        return False


def est_flotte_coulee(flotte):
    """True si tous les bateaux de la flotte sont coulés"""

    if est_bateau_coule(flotte, "porte-avions"):
        if est_bateau_coule(flotte, "croiseur"):
            if est_bateau_coule(flotte, "contre-torpilleurs"):
                if est_bateau_coule(flotte, "sous-marin"):
                    if est_bateau_coule(flotte, "torpilleur"):
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False


def analyse_tir(flotte, Ln):
    """Renvoie l'effet du tir sur la flotte parmi: touche, eau, coule, gagne"""
    flotte["tirs"].append(Ln)
    nbr_coule_apres = nombre_bateaux_coules(flotte)
    effet = "eau"
    if Ln in positions_bateaux(flotte, excepte=""):
        flotte["nbreTouche"] += 1
        if nbr_coule_apres != flotte["nbreCoule"]:
            flotte["nbreCoule"] += 1
            if est_flotte_coulee(flotte):
                effet = "gagne"
            else:
                effet = "coule"
        else:
            effet = "touche"
    flotte["effets"].append(effet)
    return effet


def liste_bateaux():
    """Renvoie la liste des bateaux par ordre alphabétique."""
    liste = ["porte-avions", 'croiseur', "contre-torpilleurs", "sous-marin", "torpilleur"]
    return sorted(liste)


def liste_survivant(flotte):
    """fonction qui renvoie une liste des bateaux non couler c'est une FONCTION EN +/ INUTILE"""
    liste_survi = list(flotte["bateaux"].keys())

    if est_bateau_coule(flotte, "porte-avions"):
        liste_survi.remove("porte-avions")
    if est_bateau_coule(flotte, "croiseur"):
        liste_survi.remove("croiseur")
    if est_bateau_coule(flotte, "contre-torpilleurs"):
        liste_survi.remove("contre-torpilleurs")
    if est_bateau_coule(flotte, "sous-marin"):
        liste_survi.remove("sous-marin")
    if est_bateau_coule(flotte, "torpilleur"):
        liste_survi.remove("torpilleur")
    return liste_survi


def nbre_survi(flotte):
    """fonction qui compte le nombre de bateaux couler FONTION EN + / INUTILE"""
    nbr = 5
    if est_bateau_coule(flotte, "porte-avions"):
        nbr -= 1
    if est_bateau_coule(flotte, "croiseur"):
        nbr -= 1
    if est_bateau_coule(flotte, "contre-torpilleurs"):
        nbr -= 1
    if est_bateau_coule(flotte, "sous-marin"):
        nbr -= 1
    if est_bateau_coule(flotte, "torpilleur"):
        nbr -= 1
    return nbr


def memorise_action_tir_sur_flotte_inconnue(flotte, case, resultat):
    flotte["effets"].append(resultat)
    flotte["tirs"].append(case)
    if resultat in ["touche", "coule", "gagne"]:
        flotte["nbreTouche"] += 1
        if resultat in ["coule", "gagne"]:
            flotte["nbreCoule"] += 1


def initialisation_flotte_par_commande_du_serveur(flotte, liste):
    for i in range(1, 6):
        flotte["bateaux"]["porte-avions"].append(liste[i])
    for i in range(7, 11):
        flotte["bateaux"]["croiseur"].append(liste[i])
    for i in range(12, 15):
        flotte["bateaux"]["contre-torpilleurs"].append(liste[i])
    for i in range(16, 19):
        flotte["bateaux"]["sous-marin"].append(liste[i])
    for i in range(20, 22):
        flotte["bateaux"]["torpilleur"].append(liste[i])
    return flotte


def envoi_flotte_via_serveur(flotte):
    bateaux = flotte["bateaux"]
    str1 = "|".join(bateaux["porte-avions"])
    str2 = "|".join(bateaux["croiseur"])
    str3 = "|".join(bateaux["contre-torpilleurs"])
    str4 = "|".join(bateaux["sous-marin"])
    str5 = "|".join(bateaux["torpilleur"])
    return "porte-avions|" + str1 + "|croiseur|" + str2 + "|contre-torpilleurs|" + str3 + "|sous-marin|" + str4 + "|torpilleur|" + str5


def sauvegarde_partie(ma_flotte, sa_flotte):
    """Pas besoin de description"""
    fd = open("sauvegarde.bin", "wb")
    pickle.dump(ma_flotte, fd)
    pickle.dump(sa_flotte, fd)
    fd.close()


def restauration_partie(ma_flotte, sa_flotte):
    """	la partie a-t-elle pu être correctement restaurée """
    if os.path.isfile("sauvegarde.bin"):
        fd = open("sauvegarde.bin", "rb")
        ma_flotte_rest = pickle.load(fd)
        sa_flotte_rest = pickle.load(fd)
        ma_flotte.update(ma_flotte_rest)
        sa_flotte.update(sa_flotte_rest)
        fd.close()
        return True
    return False


def nombre_bateaux_coules(flotte):
    bateau = flotte['bateaux']
    tir = flotte['tirs']
    coule = 0
    for elm in bateau:
        bat = flotte['bateaux'][elm]
        touche = 0
        if not (bateau[elm]):
            touche += 0
        else:
            for i in bat:
                if i in tir:
                    touche += 1
            if touche == len(bat):
                coule += 1
    return coule


def nombre_tirs_touchant_flotte(flotte):
    touche = 0
    bateaux = flotte['bateaux']
    tir = flotte['tirs']
    for i in tir:
        for elm in bateaux:
            if i in bateaux[elm]:
                touche += 1
    return touche


def tir_aleatoire(flotte):
    var = outils.case_aleatoire()
    while var in flotte['tirs']:
        var = outils.case_aleatoire()
    return var


def longueur_bateau(bateau):
    dict_longeur_bateau = {'porte-avions': 5, 'croiseur': 4, 'contre-torpilleurs': 3, 'sous-marin': 3, 'torpilleur': 2}
    return dict_longeur_bateau[bateau]


def initialisation_flotte_vide():
    return {'bateaux': {'porte-avions': [],
                        'croiseur': [],
                        'contre-torpilleurs': [],
                        'sous-marin': [],
                        'torpilleur': []},
            'tirs': [],
            'effets': [],
            'pseudo': 'anonymous',
            'nbreTouche': 0,
            'nbreCoule': 0}


def est_flotte_complete(flotte):
    bateaux = flotte["bateaux"]
    if len(bateaux["porte-avions"]) == longueur_bateau("porte-avions"):
        if len(bateaux["croiseur"]) == longueur_bateau("croiseur"):
            if len(bateaux["contre-torpilleurs"]) == longueur_bateau("contre-torpilleurs"):
                if len(bateaux["sous-marin"]) == longueur_bateau("sous-marin"):
                    if len(bateaux["torpilleur"]) == longueur_bateau("torpilleur"):
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False


def choix_flotte_aleatoire(flotte):  # A MODIFIER ,Aleatoire et il faut que les proba de chaque case soit equivalente

    liste_bateaux = ['porte-avions', 'croiseur', 'contre-torpilleurs', 'sous-marin', 'torpilleur']

    for bateau in liste_bateaux:
        position = outils.case_aleatoire()
        direction = random.choice(['horizontal', 'vertical'])
        if bateau == 'porte-avions':  # taille : 5

            emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)
            while not emplacement:
                position = outils.case_aleatoire()
                emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)

        elif bateau == 'croiseur':  # taille : 4

            emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)
            while not emplacement:
                position = outils.case_aleatoire()
                emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)

        elif bateau == 'contre-torpilleurs':  # taille : 3

            emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)
            while not emplacement:
                position = outils.case_aleatoire()
                emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)

        elif bateau == 'sous-marin':  # taille : 3
            emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)
            while not emplacement:
                position = outils.case_aleatoire()
                emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)

        elif bateau == 'torpilleur':  # taille : 2
            emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)
            while not emplacement:
                position = outils.case_aleatoire()
                emplacement = positionne_bateau_par_direction(flotte, bateau, position, direction)
    return flotte

def est_flotte_complete(flotte):

    liste_bateaux = ['porte-avions', 'croiseur', 'contre-torpilleurs', 'sous-marin', 'torpilleur']
    valide = False
    for bateau in liste_bateaux:
        if bateau == 'porte-avions':
            if not flotte['bateaux'][bateau] or len(flotte['bateaux'][bateau]) < longueur_bateau(bateau):
                return valide
        elif bateau == 'croiseur':
            if not flotte['bateaux'][bateau] or len(flotte['bateaux'][bateau]) < longueur_bateau(bateau):
                return valide
        elif bateau == 'contre-torpilleurs':
            if not flotte['bateaux'][bateau] or len(flotte['bateaux'][bateau]) < longueur_bateau(bateau):
                return valide
        elif bateau == 'sous-marin':
            if not flotte['bateaux'][bateau] or len(flotte['bateaux'][bateau]) < longueur_bateau(bateau):
                return valide
        elif bateau == 'torpilleur':
            if not flotte['bateaux'][bateau] or len(flotte['bateaux'][bateau]) < longueur_bateau(bateau):
                return valide
    valide = True
    return valide

def positionne_bateau_par_direction(flotte, bateau, case, direction):

    longueur = longueur_bateau(bateau)

    position = calcul_positions_bateau(case, direction, longueur)
    if not position:
        return False
    for case in position:
        for case2 in positions_bateaux(flotte):
            if case == case2:
                return False
    flotte['bateaux'][bateau] = position
    return True

def calcul_positions_bateau(case, direction, longueur):

    liste_cases_bateau = []

    if outils.est_case_valide(case):

        liste_cases_bateau = [case]

        if direction == 'horizontal':

            for i in range(1, longueur):
                case_supp = case[0] + str(int(case[1:]) + i)
                if outils.est_case_valide(case_supp):
                    liste_cases_bateau.append(case_supp)
                else:
                    liste_cases_bateau = []
                    return liste_cases_bateau

        elif direction == 'vertical':

            for i in range(1, longueur):
                case_supp = string.ascii_uppercase[string.ascii_uppercase[:10].index(case[0]) + i] + case[1:]
                if outils.est_case_valide(case_supp):
                    liste_cases_bateau.append(case_supp)
                else:
                    liste_cases_bateau = []
                    return liste_cases_bateau

        return liste_cases_bateau
    else:
        return liste_cases_bateau

# Programme principal pour tester vos fonctions
def main():
    pass

    # Zone test à supprimer a la fin


# print(envoi_flotte_via_serveur(initialisation_flotte_par_dictionnaire_fixe("flotte1")))
# print(est_flotte_complete(initialisation_flotte_par_dictionnaire_fixe("flotte1")))


if __name__ == '__main__':
    main()
