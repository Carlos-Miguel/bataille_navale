# Battleship

In 2018, I embarked on my first programming project, and it was a significant milestone in my journey as a developer.
This project marked the beginning of my exploration into the realm of software development, and it served as a foundation upon which I would build my coding skills and knowledge. Looking back, I can see how far I've come since that first project, but I'll always remember it as the humble beginning of my programming adventure. 
Despite the challenges I faced during the project, including time constraints that prevented me from completing it to my original vision, I managed to deliver a result. I could complete within the given timeframe, and my dedication paid off when I received a great grade for my efforts.

This experience taught me valuable lessons about time management, prioritization, and the importance of setting realistic goals in the world of programming. It reinforced the idea that the journey of a developer is not always about perfection but about continuous learning and improvement. That first project, incomplete as it may have been, fueled my passion for coding and motivated me to strive for excellence in all my future endeavors. It was a reminder that setbacks can sometimes lead to unexpected successes, and that the learning process in the world of programming is as important as the end result.

# How to play 

You can simply execute the "bataille_navale.exe" **Warning** it launches with sound.

![Landing page](img/how-to-play/homepage.png)

Then you can press any key to start the game or press 'O' to open the option menu while in the landing page. 

![Main page](img/how-to-play/main.png)

Then you can select and place your ships on the smaller grid. Once all ships are placed, just click on the opponent's grid to fire!

* Console Game

The game can also be play using console prompt launch the command : **python jeu_console.py** inside the bataille_navale folder.

![Console Game](img/how-to-play/console.png)