#! /usr/bin/env python3
# coding: UTF-8
"""
Script: terminal.py
"""
import string
import outils
import flottes
import couleurs


# Fonctions à développer selon le cahier des charges
def affiche_flotte_textuelle(flotte):
    bateaux = flotte['bateaux']
    porte = bateaux['porte-avions']
    crois = bateaux['croiseur']
    ct = bateaux['contre-torpilleurs']
    ss = bateaux['sous-marin']
    torp = bateaux['torpilleur']

    porteS = ", ".join(porte)
    print("   > porte-avions :", porteS)
    croisS = ", ".join(crois)
    print("   > croiseur :", croisS)
    ctS = ", ".join(ct)
    print("   > contre-torpilleurs :", ctS)
    ssS = ", ".join(ss)
    print("   > sous-marin :", ssS)
    torpS = ", ".join(torp)
    print("   > torpilleur :", torpS)

    return None


def affiche_flotte_2d(flotte, cachee=False):  # Affiche un tableau 2D contenant la flotte

    types_bateaux = {'porte-avions': "p",
                     'croiseur': "c",
                     'contre-torpilleurs': "q",
                     'sous-marin': "s",
                     'torpilleur': "t"}

    taille_grille = 10

    print("   |", end="")  # Affiche les colonnes
    for n in range(taille_grille):
        print(" {}".format(n + 1), end="")
    print("|")

    print("-" * 3 + "|" + "--" * taille_grille + "-|")  # Affiche la ligne séparatrice

    if not cachee:  # Affiche la flotte et les tirs
        positions_bateaux = flottes.positions_bateaux(flotte)
        for L in string.ascii_uppercase[:taille_grille]:
            print(" {} |".format(L), end="")

            for n in range(taille_grille):
                case = L + str(n + 1)

                if case in flotte['tirs']:
                    effet = flottes.analyse_tir(flotte, case)
                else:
                    effet = "sauf"

                if case in positions_bateaux.keys():
                    lettre_bateau = types_bateaux[positions_bateaux[case]]
                    if effet == "sauf":
                        print(" {}".format(lettre_bateau), end="")
                    else:
                        print(" {}".format(lettre_bateau.upper()), end="")
                else:
                    if case in flotte['tirs']:
                        print(" O", end="")
                    else:
                        print("  ", end="")
            print(' |')

    elif cachee:
        positions_bateaux = flottes.positions_bateaux(flotte)
        for L in string.ascii_uppercase[:taille_grille]:
            print(" {} |".format(L), end="")

            for n in range(taille_grille):
                case = L + str(n + 1)

                if case in flotte['tirs']:
                    effet = flottes.analyse_tir(flotte, case)
                else:
                    effet = "sauf"

                if case in positions_bateaux.keys():
                    if not effet == "sauf":
                        print(" X", end="")
                    else:
                        print("  ", end="")
                else:
                    if case in flotte['tirs']:
                        print(" O", end="")
                    else:
                        print("  ", end="")
            print(' |')
    print("-" * 3 + "|" + "--" * taille_grille + "-|")  # Affiche la ligne séparatrice


def saisie_mode_initialisation_flottes():
    mode_flotte = ['endur', 'manuel', 'manuel+aleatoire', 'aleatoires', 'restaurees']
    valide = False
    while not valide:
        print("""
Mode de choix de la flotte :
----------------------------
 1> flottes initialisées en dur
 2> flotte du joueur initialisée manuellement et flotte de l'adversaire vide
 3> flotte du joueur initialisée manuellement et flotte de l'adversaire aléatoirement
 4> flottes initialisées aléatoirement (defaut)
 5> flottes restaurées de la dernière partie sauvegardée
""")
        saisie = input("Veuillez saisir le mode de flotte: ")
        if saisie in [str(i) for i in range(1, 6)]:
            saisie = int(saisie)
            valide = True
    return mode_flotte[saisie - 1]


def choix_flotte_manuel_console(flotte):
    bateau = {3: 'porte-avions', 2: 'croiseur', 1: 'contre-torpilleurs', 4: 'sous-marin', 5: 'torpilleur'}
    commandes = ['stop']
    mot=""
    fini = False
    while not fini:
        saisie = choix_bateau_a_positionner(flotte)
        if saisie == 'stop':
            return
        else:
            print("Choisir la case de début du {}".format(bateau[saisie]))
            position = saisie_cible_valide_ou_commande(commandes)
            cases_occupes = str(flotte["bateaux"].values())

            if position in cases_occupes:
                print("-> positionnement du {} échoué".format(bateau[saisie]))
            elif position not in commandes:
                direction = saisie_direction_valide()
                flottes.positionne_bateau_par_direction(flotte, bateau[saisie], position, direction)
                fini = flottes.est_flotte_complete(flotte)
                print("-> positionnement du {} en ".format(bateau[saisie]),end="")
                liste=flotte["bateaux"][bateau[saisie]]
                for i in range(flottes.longueur_bateau(bateau[saisie])):
                    mot+=liste[i]+", "
                mot=mot[:-2]
                print(mot+" effectué")
            mot=""

            print("La flotte en 2D :")
            affiche_flotte_2d_couleurs(flotte)
            print()
    choix_bateau_a_positionner(flotte)
    return


def saisie_adresse_reseau():
    ok = False
    while not ok:
        saisie = input("Veuillez saisir l'adresse du serveur au format serveur:port : ")
        if saisie.find(":") > 0:
            repere = saisie.find(":")
            serveur = saisie[:repere]
            port = saisie[repere + 1:]
            if port.isnumeric():
                port = int(port)
                ok = True
    return [serveur, port]


# !!!!!!!!!!!! PARTI CARLOS !!!!!!!!!!!!!!!!!!!!


def saisie_cible_valide_ou_commande(commandes):
    """la case au format Ln choisie par l’utilisateur ou stop ou sauv ou les 2
        c'était pas facile facile"""
    com = ["sauv", "stop"]
    saisie = input("Saisi une case : ")
    while outils.est_case_valide(saisie) != True and saisie not in com:
        saisie = input("saisie une case ")

    return saisie


def affiche_flotte_2d_couleurs(flotte, cachee=False):
    alpha = list(string.ascii_uppercase)
    couleur_bat = {"porte-avions": "rouge",
                   "croiseur": "cyan",
                   "contre-torpilleurs": "jaune",
                   "sous-marin": "vert",
                   "torpilleur": "magenta"}

    symboles = {"vu": "#",
                "touche": "X",
                "coule": "X",
                "gagne": "X",
                "eau": "O"}

    print("   |", end="")
    for a in range(10):
        print(" {}".format(a + 1), end="")
    print("|")
    print("-" * 3 + "|" + "--" * 10 + "-|")

    if cachee == True:
        for i in range(10):
            print(" {} |".format(alpha[i]), end="")
            for carreau1 in range(10):
                carreau = alpha[i] + str(carreau1 + 1)
                if carreau in flotte["tirs"]:
                    effet = flottes.analyse_tir(flotte, carreau)
                    if effet == "eau":
                        print(" {}".format(symboles[effet]), end="")
                    else:
                        print(couleurs.COULEURS["rouge"], end="")
                        print(" {}".format(symboles[effet]), end="")
                        print(couleurs.COULEURS["blanc"], end="")
                else:
                    print("  ", end="")
            print(" |")

    if cachee == False:
        positions = flottes.positions_bateaux(flotte)
        for i in range(10):
            print(" {} |".format(alpha[i]), end="")
            for carreau1 in range(10):
                carreau = alpha[i] + str(carreau1 + 1)
                if carreau in flotte["tirs"]:
                    effet = flottes.analyse_tir(flotte, carreau)
                else:
                    effet = "vu"
                if carreau in positions.keys():
                    couleur = couleur_bat[positions[carreau]]
                    exec('print(couleurs.COULEURS["{}"],end="")'.format(couleur))
                    print(" {}".format(symboles[effet]), end="")
                    print(couleurs.COULEURS["blanc"], end="")
                else:
                    if carreau in flotte["tirs"]:
                        print(" O", end="")
                    else:
                        print("  ", end="")
            print(" |")

    print("-" * 3 + "|" + "--" * 10 + "-|")


def saisie_direction_valide():
    """	la direction saisie sous la forme d’une chaine de caractères parmi « horizontal » ou « vertical »"""
    choix = input("Choisie une direction valide parmi 'horizontal' ou'vertical': ")
    while choix != "horizontal" and choix != "vertical" and choix not in ["h", "v", "H", "V"]:
        choix = input("Choisie une direction valide parmi 'horizontal' ou'vertical'")
    if choix in ["h", "H"]:
        return "horizontal"
    if choix in ["v", "V"]:
        return "vertical"
    return choix


def affiche_flotte_inconnue_2d(flotte):
    alpha = list(string.ascii_uppercase)

    print("   |", end="")
    for a in range(10):
        print(" {}".format(a + 1), end="")
    print("|")
    print("-" * 3 + "|" + "--" * 10 + "-|")

    positions_bateaux = flottes.positions_bateaux(flotte)
    for i in range(10):
        print(" {} |".format(alpha[i]), end="")

        for carreau1 in range(10):
            case = alpha[i] + str(carreau1 + 1)
            if case in flotte['tirs']:
                effet = flottes.analyse_tir(flotte, case)
            else:
                effet = "rien"
            if case in positions_bateaux.keys():
                if effet != "rien":
                    print(" X", end="")
                else:
                    print("  ", end="")
            else:
                if case in flotte['tirs']:
                    print(" O", end="")
                else:
                    print("  ", end="")
        print(' |')
    print("-" * 3 + "|" + "--" * 10 + "-|")
    print("")
    touche = int(flotte["nbreTouche"] / 2)
    coule = flotte["nbreCoule"]
    print("Indicateurs : {} touches / {} bateau(x) coulé(s)".format(touche, coule))


def choix_bateau_a_positionner(flotte):
    bateaux = flotte["bateaux"]
    menu = {}
    if bateaux["contre-torpilleurs"] == []:
        menu['   1)'] = "contre-torpilleurs (3 cases), actuellement non positionné"
    else:
        menu['   1)'] = "contre-torpilleurs (3 cases), actuellement en " + ", ".join(bateaux.get("contre-torpilleurs"))

    if bateaux["croiseur"] == []:
        menu["   2)"] = "croiseur (4 cases), actuellement non positionné"
    else:
        menu["   2)"] = "croiseur (4 cases), actuellement en " + ", ".join(bateaux.get("croiseur"))

    if bateaux["porte-avions"] == []:
        menu["   3)"] = "porte-avions (5 cases), actuellement non positionné"
    else:
        menu["   3)"] = "porte-avions (5 cases), actuellement en " + ", ".join(bateaux.get("porte-avions"))

    if bateaux["sous-marin"] == []:
        menu["   4)"] = "sous-marin (3 cases), actuellement non positionné"
    else:
        menu["   4)"] = "sous-marin (3 cases), actuellement en " + ", ".join(bateaux.get("sous-marin"))
    if bateaux["torpilleur"] == []:
        menu["   5)"] = "torpilleur (2 cases), actuellement non positionné"
    else:
        menu["   5)"] = "torpilleur (2 cases), actuellement en " + ", ".join(bateaux.get("torpilleur"))
    print("Choisir le bateau à (re)positionner parmi :")

    oui = False
    options = menu.keys()
    for key in options:
        print(key, menu[key])
    print("Taper stop pour stopper le choix de la flotte")
    while oui == 0:

        choix = input("Choisis ton bateau: ")
        while choix != "stop" and choix not in ["1", "2", "3", "4", "5"]:
            choix = input("Choisis ton bateau: ")
        if choix == "1":
            return 1
        if choix == '2':
            return 2
        if choix == "3":
            return 3
        if choix == "4":
            return 4
        if choix == "5":
            return 5
        if choix == "stop":
            if flottes.est_flotte_complete(flotte):
                return "stop"
            else:
                pass







def saisie_mode_choix_jeu():
    message = "\nMode de choix du jeu :\n" + \
              "----------------------\n" + \
              " 1> jeu entièrement manuel [utile pour le debug] (choix par defaut)\n" + \
              " 2> jeu automatique contre l'ordinateur\n" + \
              " 3> jeu automatique avancé contre l'ordinateur (tir avec IA)\n" + \
              " 4> jeu en réseau\n"
    print(message)
    choix = input("x")
    while choix not in ["1", "2", "3", "4", "manuel", "auto", "reseau", "ia"]:
        print(message)
        choix = input()
    if choix in ["1", "manuel"]:
        return "manuel"
    if choix in ["2", "auto"]:
        return "auto"
    if choix in ["3", "ia"]:
        return 'ia'
    if choix in ["4", "reseau"]:
        return "reseau"


def saisie_pseudo():
    pseudo = input("Votre pseudo: ")
    while pseudo == "":
        pseudo = input("Votre pseudo: ")
    return pseudo


# Programme principal pour tester vos fonctions
def main():
    #choix_flotte_manuel_console(flottes.initialisation_flotte_vide())
    pass

if __name__ == '__main__':
    main()
