#! /usr/bin/env python3
# coding: UTF-8
"""
Script: outil.py
"""
import random
import string
import os


# Fonctions à développer selon le cahier des charges

def est_case_valide(case):
    alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    beta = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
    if case[0] in alpha and case[1::] in beta:
        return True
    else:
        return False


def indices_vers_case_ln(indices):
    lignes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    lignes_ap = indices[0]
    lignefinal = str(lignes[lignes_ap])
    col_ap = indices[1] + 1
    if col_ap==11:
        col_ap-=1
    case = lignefinal + str(col_ap)
    return case


def case_ln_vers_indices(case):
    prem = case[0]
    dern = int(case[1::])
    ligncase = []
    ligncase.append(string.ascii_uppercase.find(prem))
    ligncase.append(dern - 1)
    return ligncase


def joueur_aleatoire():
    jourlist = ['moi', 'adversaire']
    joueur = random.choice(jourlist)
    return joueur


def case_aleatoire():
    num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    cose = []
    caseAl = random.choice(num)
    cose.append(caseAl)
    caseBe = random.choice(num)
    cose.append(caseBe)
    case = indices_vers_case_ln(cose)
    return case


def affichage_statistiques_joueurs():
    """Il y a bcp de choses qui servent à rien mais ça enleve les warning donc j'ai mis
    La fonction renvoie l'affichage des stats des joueurs enregistrer sur le fichier statistique.txt"""
    joueurs = ''
    plus_long = ''
    tete = "+---------+----------------+-----------+----------+----------+" + "\n" \
           + "| Joueurs | Parties jouées | Victoires | Défaites | Abandons |" + "\n" \
           + "+=========+================+===========+==========+==========+" + "\n"
    liste_pseudo = []
    taille_pseudo = []
    taille = False
    if os.path.isfile("statistiques.txt"):  # verifier si le fichier existe

        fd = open("statistiques.txt", "r")

        for lignes in fd:  # permet de recup tout les pseudos dans une liste
            champs = lignes.split(";")
            liste_pseudo = liste_pseudo + champs
        liste_pseudo = liste_pseudo[::4]

        for elm in liste_pseudo:  # regarde quelle elm est le plus long
            taille_pseudo.append(len(elm))
            plus_long = max(taille_pseudo)
            if plus_long > 7:
                taille = True
        fd.seek(0)  # replace le curseur au debut
        for lignes in fd:  # parcours tout le fichier et fait l'affichage
            champs = lignes.split(";")
            pseudo = champs[0]
            nj = int(champs[1])
            ng = int(champs[2])
            np = int(champs[3])
            na = int(nj - ng - np)
            if taille:
                ecart = (plus_long + 1) - len(pseudo)
                ecart_tiret = plus_long + 1

                if len(pseudo) < plus_long:
                    tete = "+-" + "-" * ecart_tiret + "+----------------+-----------+----------+----------+" + "\n" \
                           + "| Joueurs" \
                           + " " * (ecart_tiret - len("joueurs")) \
                           + "| Parties jouées | Victoires | Défaites | Abandons |" + "\n" \
                           + "+=" + "=" * ecart_tiret + "+================+===========+==========+==========+" + "\n"
                    bas = "+-" + "-" * ecart_tiret + "+----------------+-----------+----------+----------+" + "\n"
                    joueurs = joueurs + "| " \
                              + pseudo + " " * ecart \
                              + "| {: <15}| {: <10}| {: <9}| {: <9}|".format(nj, ng, np, na) + "\n" + bas

                if len(pseudo) == plus_long:
                    bas = "+-" + "-" * (plus_long + 1) + "+----------------+-----------+----------+----------+" + "\n"
                    joueurs = joueurs + "| " + pseudo + " " \
                              + "| {: <15}| {: <10}| {: <9}| {: <9}|".format(nj, ng, np, na) + "\n" + bas

            if not taille:
                bas = "+---------+----------------+-----------+----------+----------+" + "\n"
                joueurs = joueurs + (
                        "| {: <8}| {: <15}| {: <10}| {: <9}| {: <9}|".format(pseudo, nj, ng, np, na) + "\n") + bas
        tout = tete + joueurs
        return tout

    return ""


def ajoute_statistiques_joueur(pseudo, resultat):
    tout = ""
    if os.path.isfile("statistiques.txt"):  # Verifie si le fichier existe
        fd0 = open("statistiques.txt", "r")
        for ligne in fd0:
            champs = ligne.split(";")
            pseudo_sauv = champs[0]
            if pseudo_sauv == '\n':
                pass
            else:
                NJ = str(champs[1])
                NG = str(champs[2])
                NP = str(champs[3])
                if pseudo_sauv == pseudo:
                    NJ = int(NJ) + 1
                    NJ=str(NJ)
                    if resultat == "gagne":
                        NG = int(NG) + 1
                        tout = tout + pseudo_sauv + ";" + NJ + ";" + str(NG) + ";" + NP
                    elif resultat == "perd":
                        NP = int(NP) + 1
                        tout = tout + pseudo_sauv + ";" + NJ + ";" + NG + ";" + str(NP) +"\n"
                    elif resultat == "abandonne":
                            tout = tout + pseudo_sauv + ";" + NJ + ";" + NG + ";" + NP
                else:
                    tout = tout + pseudo_sauv + ";" + NJ + ";" + NG + ";" + NP
        fd0.close()
        fd1 = open("statistiques.txt", "w")
        if pseudo in tout:
            fd1.write(tout)
        else:
            fd1.write(tout)
            NJ = 1
            NG = 0
            NP = 0
            if resultat == "gagne":
                NG += 1
            elif resultat == "perd":
                NP += 1
            elif resultat == "abandonne":
                pass
            fd1.write("{};{};{};{}".format(pseudo, NJ, NG, NP))
            fd1.write("\n")
        fd1.close()
    else:
        fd1 = open("statistiques.txt", "w")
        NJ = 1
        NG = 0
        NP = 0
        if resultat == "gagne":
            NG += 1
        elif resultat == "perd":
            NP += 1
        elif resultat == "abandonne":
            pass
        fd1.write("{};{};{};{}".format(pseudo, NJ, NG, NP))
        fd1.write("\n")
        fd1.close()


# Programme principal pour tester vos fonctions
def main():
    #print(indices_vers_case_ln([7,9]))
    pass




if __name__ == '__main__':
    main()
