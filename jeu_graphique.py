"""Par CORREIA DE ALMEIDA Carlos Miguel
Super mal codé, bonne chance pour relire have fun.
"""

from tkinter import *  # Graphique
from tkinter import ttk
from PIL import Image, ImageTk  # Pour les images
from winsound import PlaySound, SND_FILENAME, SND_LOOP, SND_ASYNC  # Musique
import time
import string

import outils
import flottes
import ia


#import os
#import random


def play_music():
    if Musique:
        PlaySound('son/Background.wav', SND_FILENAME | SND_LOOP | SND_ASYNC)
    # Westworld - Main Theme

def play_music2():
    global Musique
    Musique=True
    play_music()

def play_victory():
    if Musique:
        PlaySound('son/Win.wav', SND_FILENAME | SND_ASYNC)
    # Britney Spears - Oops!...I Did It Again


def play_defaite():
    if Musique:
        PlaySound('son/Lose.wav', SND_FILENAME | SND_ASYNC)
    # Blonde Redhead - For the Damaged Coda


def stop_music():
    global Musique
    Musique = False
    PlaySound(None, SND_FILENAME)


def raise_frame(frame):
    frame.tkraise()
    frame.focus()


class background(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid(sticky=N + S + E + W)
        self.image = Image.open("img/bateau2.jpg")
        self.img_copy = self.image.copy()

        self.background_image = ImageTk.PhotoImage(self.image)

        self.background = Label(self, image=self.background_image)
        self.background.grid(row=0, column=0, sticky="nsew")
        self.background.grid_rowconfigure(0, weight=1)
        self.background.grid_columnconfigure(0, weight=1)
        self.master = master
        self.master.bind('<Configure>', self._resize_image)

    def _resize_image(self, event):
        global taille_ecran_x, taille_ecran_y
        taille_ecran_x = self.master.winfo_width()
        taille_ecran_y = self.master.winfo_height()

        self.image = self.img_copy.resize((taille_ecran_x-3, taille_ecran_y-3))

        self.background_image = ImageTk.PhotoImage(self.image)
        self.background.configure(image=self.background_image)


def change(event):
    # menu option
    raise_frame(f2)


def lancer_jeu(event):
    print("Bienvenue {} pour commencer placer vos bateaux !".format(pseudo.get()))
    print("Pour avoir des informations appuyer sur 'H'.\n")
    root.resizable(False, False)
    raise_frame(frame_jeux)
    centrer_ma_flotte()
    centrer_sa_flotte()
    menu_bat()



def bataille_auto(case):
    global partie_terminer, partie_perdu, sa_flotte,ma_flotte
    #print(ma_flotte)
    if case in sa_flotte["tirs"]:
        # print("Debug, moi: ", ma_flotte)
        # print("Debug, lui:", sa_flotte)
        if sa_flotte["nbreCoule"]==5:
            #print("oui 2")
            case = case_vers_coordonnee(case, taille_sa_flotte_x, taille_sa_flotte_y)
            can_sa_flotte.create_oval(case[0], case[1], case[2], case[3] - 5, fill="gold")
            en_vie["text"] = "Nombre de bateaux à couler : {}".format(flottes.nbre_survi(sa_flotte))
            partie_terminer = True
            print("Gagner !\n")
            play_victory()
            outils.ajoute_statistiques_joueur(pseudo.get(),"gagne")
            return
    else:
        effet = flottes.analyse_tir(sa_flotte, case)
        affichage_effet_sa_flotte(effet, case, can_sa_flotte)
        if sa_flotte["nbreCoule"]==5:
            #print("oui 1")
            case = case_vers_coordonnee(case, taille_sa_flotte_x, taille_sa_flotte_y)
            can_sa_flotte.create_oval(case[0], case[1], case[2], case[3] - 5, fill="goldenrod1")
            en_vie["text"] = "Nombre de bateaux à couler : {}".format(flottes.nbre_survi(sa_flotte))
            partie_terminer = True
            print("Gagner !\n")
            play_victory()
            outils.ajoute_statistiques_joueur(pseudo.get(), "gagne")
            return
        if IA is True:
            saisie_ia=ia.auto(ma_flotte)
        elif IA is False:
            saisie_ia = ia.auto_pas_jeu_concours(ma_flotte)
        effet2 = flottes.analyse_tir(ma_flotte, saisie_ia)
        affichage_effet_ma_flotte(effet2, saisie_ia, can_ma_flotte)
        if ma_flotte["nbreCoule"] == 5:
            #print("oui 5")
            partie_terminer = True
            partie_perdu = True
            print("Perdu ! ")
            play_defaite()
            outils.ajoute_statistiques_joueur(pseudo.get(), "perd")
            return

def affichage_effet_sa_flotte(effet, case, can):
    global partie_terminer
    # print(effet)
    if effet == "eau":
        case = case_vers_coordonnee(case, taille_sa_flotte_x, taille_sa_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3] - 5, fill="dodgerblue")
    if effet == "touche":
        case = case_vers_coordonnee(case, taille_sa_flotte_x, taille_sa_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3] - 5, fill="red")
    if effet == "coule":
        case = case_vers_coordonnee(case, taille_sa_flotte_x, taille_sa_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3] - 5, fill="red4")
        en_vie["text"]="Nombre de bateaux à couler : {}".format(flottes.nbre_survi(sa_flotte))
    if effet == "gagne":
        case = case_vers_coordonnee(case, taille_sa_flotte_x, taille_sa_flotte_y)
        can_sa_flotte.create_oval(case[0], case[1], case[2], case[3] - 5, fill="goldenrod1")
        en_vie["text"] = "Nombre de bateaux à couler : {}".format(flottes.nbre_survi(sa_flotte))
        partie_terminer = True

        play_victory()
        outils.ajoute_statistiques_joueur(pseudo.get(), "gagne")
        return


def affichage_effet_ma_flotte(effet, case, can):
    global partie_terminer, partie_perdu
    if effet == "eau":
        case = case_vers_coordonnee(case, taille_ma_flotte_x, taille_ma_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3], fill="dodgerblue")
    if effet == "touche":
        case = case_vers_coordonnee(case, taille_ma_flotte_x, taille_ma_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3], fill="red")
    if effet == "coule":
        case = case_vers_coordonnee(case, taille_ma_flotte_x, taille_ma_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3], fill="red4")
    if effet == "gagne":
        case = case_vers_coordonnee(case, taille_ma_flotte_x, taille_ma_flotte_y)
        can.create_oval(case[0], case[1], case[2], case[3], fill="gold")
        partie_terminer = True
        partie_perdu = True
        play_defaite()
        #bug souvent l'effet gagne

def change_resolutions(value):
    global taille_ecran_x, taille_ecran_y
    if value == "VGA":
        taille_ecran_x = 640
        taille_ecran_y = 480
    if value == "1080p":
        taille_ecran_x = 1920
        taille_ecran_y = 1020
    if value == "1440p":
        taille_ecran_x = 2560
        taille_ecran_y = 1440
    if value == "720p":
        taille_ecran_x = 1280
        taille_ecran_y = 720
    # print(taille_ecran_x, taille_ecran_y)
    centrerfenetre(frame_fond)


def refresh():
    root.destroy()


def centrerfenetre(fen):
    # centrer

    fen.update_idletasks()
    posX = (ecran_x // 2) - (taille_ecran_x // 2)
    posY = (ecran_y // 2) - (taille_ecran_y // 2)
    geo = "{}x{}+{}+{}".format(taille_ecran_x, taille_ecran_y, posX, posY)
    root.geometry(geo)


def centrer_ma_flotte():
    global taille_ma_flotte_x, taille_ma_flotte_y, can_ma_flotte
    # variable
    taille_ma_flotte_x = int(round(taille_ecran_x / 2) * (4 / 6))
    taille_ma_flotte_y = int(round(taille_ecran_y * (3 / 6)))
    hauteur_legend = int(round(taille_ecran_y * (1 / 16)))
    largeur_legend = int(round(taille_ecran_x / 24))
    posX = int(round(taille_ecran_x / 2 + (taille_ecran_x * (1 / 12))))
    posY = int(round(taille_ecran_y / 9))
    var = int(round(taille_ecran_y / 60))

    # Legend
    legend = Canvas(frame_jeux, width=taille_ma_flotte_x, height=hauteur_legend)
    legend.pack()
    legend.place(x=posX, y=posY / 2)
    for i in range(10):
        legend.create_text((taille_ma_flotte_x / 10) * i + (taille_ma_flotte_x / 10) / 2, hauteur_legend / 2,
                           text=i + 1, font=("Purisa", var))

    legend2 = Canvas(frame_jeux, width=largeur_legend, height=taille_ma_flotte_y)
    legend2.pack()
    legend2.place(x=posX - (taille_ma_flotte_x / 10), y=posY)
    for i in range(10):
        legend2.create_text(largeur_legend / 2, (taille_ma_flotte_y / 10) * (i + 1) - (taille_ma_flotte_y / 10) / 2,
                            text="{}".format(alpha[i]), font=("Purisa", var))

    # grille
    can_ma_flotte = Canvas(frame_jeux, width=taille_ma_flotte_x, height=taille_ma_flotte_y, bg="Royal Blue")
    can_ma_flotte.pack()
    can_ma_flotte.place(x=posX, y=posY)
    can_ma_flotte.update_idletasks()
    can_ma_flotte.bind("<Button 1>", clic_gauche2)
    dessiner_grille(taille_ma_flotte_x, taille_ma_flotte_y, can_ma_flotte)


def menu_bat():
    global can_choix
    posX = int(round((taille_ecran_x / 2) + ((taille_ecran_x * (1 / 20)))))
    can_choix = Canvas(frame_jeux, width=taille_ma_flotte_x, height=taille_ma_flotte_y * 1 / 3)
    can_choix.pack()
    can_choix.place(x=posX, y=taille_ecran_y - (5 * taille_ecran_y / 14))

    style = ttk.Style()
    style.map("TButton",
              foreground=[('pressed', 'green'), ('active', 'Royalblue1')],
              background=[('pressed', '!disabled', 'black'), ('active', 'white')]
              )
    ttk.Button(can_choix, text='Porte-Avions (5 cases)', command=lambda: choix_bat_dire(1),
               width=25).pack()
    ttk.Button(can_choix, text='Croiseur (4 cases)', command=lambda: choix_bat_dire(2), width=25).pack()
    ttk.Button(can_choix, text='Contre-Torpilleurs (3 cases)', command=lambda: choix_bat_dire(3),
               width=25).pack()
    ttk.Button(can_choix, text='Sous-Marin (3 cases)', command=lambda: choix_bat_dire(4), width=25).pack()
    ttk.Button(can_choix, text='Torpilleur (2 cases)', command=lambda: choix_bat_dire(5), width=25).pack()
    ttk.Button(can_choix, text='Horizontal', command=lambda: choix_bat_dire(6), width=20).pack(side=LEFT)
    ttk.Button(can_choix, text="Verticale", command=lambda: choix_bat_dire(7), width=20).pack(side=RIGHT)

def choix_bat_dire(id):
    global porte_avions, croiseur, contre_torpilleurs, sous_marin, torpilleur, horizontal, vertical
    if id == 1:
        porte_avions = True
        croiseur = False
        contre_torpilleurs = False
        sous_marin = False
        torpilleur = False
    if id == 2:
        porte_avions = False
        croiseur = True
        contre_torpilleurs = False
        sous_marin = False
        torpilleur = False
    if id == 3:
        porte_avions = False
        croiseur = False
        contre_torpilleurs = True
        sous_marin = False
        torpilleur = False
    if id == 4:
        porte_avions = False
        croiseur = False
        contre_torpilleurs = False
        sous_marin = True
        torpilleur = False
    if id == 5:
        porte_avions = False
        croiseur = False
        contre_torpilleurs = False
        sous_marin = False
        torpilleur = True
    if id == 6:
        horizontal = True
        vertical = False
    if id == 7:
        horizontal = False
        vertical = True


def positionner_bat(case):
    bateau = ""
    direction = "horizontal"
    if horizontal:
        direction = "horizontal"
    if vertical:
        direction = "vertical"
    try:
        if porte_avions:
            bateau = "porte-avions"
        if croiseur:
            bateau = "croiseur"
        if contre_torpilleurs:
            bateau = "contre-torpilleurs"
        if sous_marin:
            bateau = "sous-marin"
        if torpilleur:
            bateau = "torpilleur"
        flottes.positionne_bateau_par_direction(ma_flotte, bateau, case, direction)
        can_ma_flotte.update_idletasks()
    except:
        print("Selectionnez votre bateaux et placer le!")


def centrer_sa_flotte():
    global taille_sa_flotte_x
    global taille_sa_flotte_y
    global can_sa_flotte
    global en_vie
    # variable
    taille_sa_flotte_x = int(round(5 * taille_ecran_x / 12))
    taille_sa_flotte_y = int(round(taille_ecran_y * (6 / 8)))

    hauteur_legend = int(round(taille_ecran_y * (1 / 16)))
    largeur_legend = int(round(taille_ecran_x / 24))
    posX = int(round(taille_ecran_x / 15))
    posY = int(round(taille_ecran_y / 7))
    var = int(round(taille_ecran_y / 60))

    # Legend
    legend = Canvas(frame_jeux, width=taille_sa_flotte_x, height=hauteur_legend)
    legend.pack()
    legend.place(x=posX, y=posY / 3)
    for i in range(10):
        legend.create_text((taille_sa_flotte_x / 10) * i + (taille_sa_flotte_x / 10) / 2, hauteur_legend / 2,
                           text=i + 1, font=("Purisa", var))

    legend2 = Canvas(frame_jeux, width=largeur_legend, height=taille_sa_flotte_y)
    legend2.pack()
    legend2.place(x=posX / 3, y=posY - 22)
    for i in range(10):
        legend2.create_text(largeur_legend / 2, (taille_sa_flotte_y / 10) * (i + 1) - (taille_sa_flotte_y / 10) / 2,
                            text="{}".format(alpha[i]), font=("Purisa", var))

    # grille
    can_sa_flotte = Canvas(frame_jeux, width=taille_sa_flotte_x, height=taille_sa_flotte_y, bg="RoyalBlue3")
    can_sa_flotte.pack()
    can_sa_flotte.place(x=posX, y=posY - 22)
    can_sa_flotte.update_idletasks()
    can_sa_flotte.bind("<Button 1>", clic_gauche)
    dessiner_grille(taille_sa_flotte_x, taille_sa_flotte_y, can_sa_flotte)
    # Text
    l=Label(frame_jeux,width=largeur_legend,height=int(round(hauteur_legend/10)))
    l.pack()
    l.place(x=posX,y=int(round(posY*(19/3))))
    en_vie=ttk.Label(l,text="Nombre de bateaux à couler : 5")
    en_vie.pack()


def dessiner_grille(largeur, hauteur, can_ma_flotte):
    # verticale
    for i in range(10):
        can_ma_flotte.create_line((largeur / 10) * i, 0, (largeur / 10) * i, hauteur + 2, fill="black", width=1)
    can_ma_flotte.create_line(1, 1, 1, hauteur + 2, fill="black", width=8)
    can_ma_flotte.create_line(largeur, 0, largeur, hauteur + 2, fill="black", width=5)
    # horizontale
    for i in range(10):
        can_ma_flotte.create_line(0, (hauteur / 10) * i, largeur + 2, (hauteur / 10) * i, fill="black", width=1)
    can_ma_flotte.create_line(1, 1, largeur + 2, 1, fill="black", width=10)
    can_ma_flotte.create_line(1, hauteur, largeur + 2, hauteur, fill="black", width=5)


def clic_gauche(eventorigin):
    # saflotte
    x = eventorigin.x
    y = eventorigin.y
    # print(x, y)
    if flotte_placer is False:
        print("Placer vos bateaux!")
    if flotte_placer is True and partie_terminer is False:
        bataille_auto(coordonnee_vers_case(x, y, taille_sa_flotte_x, taille_sa_flotte_y))
    if partie_perdu:
        affiche_flotte(sa_flotte, taille_sa_flotte_x, taille_sa_flotte_y,can_sa_flotte)


def clic_gauche2(eventorigin):
    global flotte_placer
    # maflotte
    x = eventorigin.x
    y = eventorigin.y
    # print(x, y)
    if partie_terminer is True:
        print("C'est fini :( \nLa partie recommence dans un instant :D\n")
        time.sleep(2)
        variable_pour_rejouer()
        lancer_jeu(lambda: eventorigin)

    elif flottes.est_flotte_complete(ma_flotte):
        flotte_placer = True
        print("La partie à commencer !")

    else:
        positionner_bat(coordonnee_vers_case(x, y, taille_ma_flotte_x, taille_ma_flotte_y))
        affiche_flotte(ma_flotte, taille_ma_flotte_x, taille_ma_flotte_y, can_ma_flotte)
        can_ma_flotte.update_idletasks()
        if flottes.est_flotte_complete(ma_flotte):
            flotte_placer = True

def coordonnee_vers_case(x, y, taille_flotte_x, taille_flotte_y):
    liste = []
    case_x = int(round((taille_flotte_x / 10)))
    case_y = int(round((taille_flotte_y / 10)))
    nb = -1
    nb2 = -1
    oui = False
    oui2 = False
    i = 0
    i2 = 0
    while not oui:
        if nb <= x <= (nb + case_x):
            oui = True

        else:
            nb += case_x
            i += 1
    while not oui2:
        if nb2 <= y <= (nb2 + case_y):
            oui2 = True
        else:
            nb2 += case_y
            i2 += 1
    liste.insert(1, i)
    liste.insert(0, i2)
    case = outils.indices_vers_case_ln(liste)
    return case
    # case_vers_coordonnee(case, taille_flotte_x, taille_flotte_y)


def case_vers_coordonnee(case, taille_flotte_x, taille_flotte_y):
    liste = []
    indice = outils.case_ln_vers_indices(case)

    case_x = int(round((taille_flotte_x / 10)))
    case_y = int(round((taille_flotte_y / 10)))
    coord_x = (round(indice[1] * case_x) + 5)
    coord_y = (round(indice[0] * case_y) + 5)
    fin_x = (coord_x + case_x - 10)
    fin_y = (coord_y + case_y - 10)
    liste.insert(0, coord_x)
    liste.insert(1, coord_y)
    liste.insert(2, fin_x)
    liste.insert(3, fin_y)
    return liste


def affiche_flotte(flotte, taille_flotte_x, taille_flotte_y, can):
    global bat
    try:
        for i in range(len(bat)):
            can.delete(bat[i])
    except:
        pass
    bat = []
    position = flottes.positions_bateaux(flotte).keys()
    position = list(position)
    for i in range(len(position)):
        case = case_vers_coordonnee(position[i], taille_flotte_x, taille_flotte_y)
        bat.append(can.create_oval(case[0], case[1], case[2], case[3], fill="black"))
    can_ma_flotte.update_idletasks()

def variable_pour_rejouer():
    global flotte_placer,horizontal,vertical,partie_terminer,partie_perdu, sa_flotte,ma_flotte
    sa_flotte = flottes.initialisation_flotte_vide()
    sa_flotte = flottes.choix_flotte_aleatoire(sa_flotte)
    ma_flotte = flottes.initialisation_flotte_vide()
    flotte_placer = False
    horizontal = True
    vertical = False
    partie_terminer = False
    partie_perdu = False
    play_music()

def afficher_stat():
    print(outils.affichage_statistiques_joueurs())

def check_fermeture():
    if partie_terminer is False:
        outils.ajoute_statistiques_joueur(pseudo.get(),"abandonne")
    refresh()

def tuto(eventorigin):
    print("Pour jouer c'est facile :"
          "\nPlacez vos bateaux en les sélectionnant mais attention !!!\n"
          "Une fois le dernier bateau posée la partie commence.\n"
          "Quand tous les bateaux sont posées il ne reste plus qu'à tirer sur la flotte adverse !\n"
          "Si vous aimer un peu le challenge le mode IA concours est disponible en appuyant sur 'i' avant de placer vos bateaux.\n"
          "Autre chose tant que j'y suis.\n"
          "En fin de partie vous pouvez afficher la flotte adverse en cliquant sur la flotte adverse ou bien rejouer en cliquant sur votre flotte !\n"
          "\nVotez pour moi ;)")

def choix_mode(eventorigin):
    global IA
    IA= True
    print("L'IA concours est activé !")


root = Tk()

# variables
ecran_x = int(root.winfo_screenwidth())
ecran_y = int(root.winfo_screenheight())

alpha = string.ascii_uppercase
Musique = True
flotte_placer = False
horizontal = True
vertical = False
partie_terminer = False
partie_perdu = False
IA=False


taille_sa_flotte_x = 0
taille_sa_flotte_y = 0
taille_ma_flotte_x = 0
taille_ma_flotte_y = 0

sa_flotte = flottes.initialisation_flotte_vide()
sa_flotte = flottes.choix_flotte_aleatoire(sa_flotte)
#flotte_chercher=flottes.initialisation_flotte_vide()
ma_flotte = flottes.initialisation_flotte_vide()
# ma_flotte = flottes.choix_flotte_aleatoire(ma_flotte)

taille_ecran_x = 1280
taille_ecran_y = 720


root.title("Bataille Navale Xtrem Edition 2019")

root.minsize(640, 480)

# Initialisation Frames
frame_fond = background(root)
frame_fond.grid(row=0, column=0, sticky="nsew")
frame_fond.grid_rowconfigure(0, weight=1)
frame_fond.grid_columnconfigure(0, weight=1)

f2 = Frame(root)

frame_jeux = Frame(root)

for frame in (frame_fond, f2, frame_jeux):
    frame.grid(row=0, column=0, sticky="news")

# Menu Principale
frame_fond.bind("<Key>", lancer_jeu)
frame_fond.bind("<o>", change)
frame_fond.bind("<O>", change)
frame_fond.focus()

frame_jeux.bind("<H>",tuto)
frame_jeux.bind("<h>",tuto)


frame_jeux.bind("<I>",choix_mode)
frame_jeux.bind("<i>",choix_mode)
# Menu Options

v = IntVar()
v2 = IntVar()
Label(f2, text='Options',height=2, bg="RoyalBlue3").pack(fill=X)
Label(f2, text="Résolutions :").pack(pady=3)
Radiobutton(f2, activebackground="spring green", text="VGA", variable=v, value="VGA", indicatoron=False, width=10,
            command=lambda: change_resolutions("VGA")).pack(pady=3)
Label(f2, text="Recommandé :").pack()
Radiobutton(f2, activebackground="spring green", text="720p", variable=v, value=0, indicatoron=False, width=10,
            command=lambda: change_resolutions("720p")).pack(pady=3)
Radiobutton(f2, activebackground="spring green", text="1080p", variable=v, value="1080p", indicatoron=False, width=10,
            command=lambda: change_resolutions("1080p")).pack(pady=3)

Radiobutton(f2, activebackground="spring green", text="WQ-HD \n alias faux 4K", variable=v, value="1440p",
            indicatoron=False, width=10,
            command=lambda: change_resolutions("1440p")).pack(pady=3)

Label(f2, text="  ").pack()
Label(f2, text="Son :").pack()
Radiobutton(f2, activebackground="spring green", text="Activer", variable=v2, value="son", indicatoron=False, width=10,
            command=play_music2).pack(pady=3)
Radiobutton(f2, activebackground="spring green", text="Couper", variable=v2, value="son2", indicatoron=False, width=10,
            command=stop_music).pack()
Label(f2, text="  ").pack()
Label(f2, text=" Votre pseudo :").pack()
pseudo=StringVar()
pseudo.set("Invité")
Entry(f2,textvariable=pseudo).pack()
Button(f2, text="Afficher les stats sur la console", command=afficher_stat).pack(pady=5)
Button(f2, text='Retour Menu', command=lambda: raise_frame(frame_fond)).pack(pady=25, side=BOTTOM)

# Frame Jeux
#Button(frame_jeux, text='Retour Écran Principale ', command=lambda: raise_frame(frame_fond)).pack(pady=20, side=BOTTOM)
Label(frame_jeux, text='Bataille vs Ordi').pack(fill=X)
Label(frame_jeux, text='Adverse').pack(anchor=NW)
Label(frame_jeux, text='Vous      ').pack(anchor=NE)

play_music()
raise_frame(frame_fond)
centrerfenetre(frame_fond)
root.iconbitmap('img/icon.ico')
root.protocol("WM_DELETE_WINDOW",check_fermeture)
root.mainloop()
