"""Par Carlos Miguel CORREIA DE ALMEIDA"""

import flottes
import outils


def auto(flotte_escomptee):
    tir = ""
    indice_final = []
    # print(flotte_escomptee["tirs"])
    if flottes.est_bateau_coule(flotte_escomptee, "torpilleur") is False or flottes.est_bateau_coule(flotte_escomptee, "torpilleur") is True:
        try:
            if flotte_escomptee["effets"][-1] == "touche":
                #attaque droite
                dernier_tir = flotte_escomptee["tirs"][-1]
                indice_der = outils.case_ln_vers_indices(dernier_tir)
                indice_der[1] = indice_der[1] + 1
                if indice_der[1] > 9:
                    #sinon gauche
                    indice_der[1] = indice_der[1] - 2
                if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                    #haut
                    indice_der = outils.case_ln_vers_indices(dernier_tir)
                    indice_der[0]=indice_der[0]-1
                    if indice_der[0]<0:
                        #sinon bas
                        indice_der[0]=indice_der[0]+2
                    if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                        pass
                else:
                    return outils.indices_vers_case_ln(indice_der)

            if flotte_escomptee["effets"][-2] == "touche" and flotte_escomptee["effets"][-1] != "coule":
                # attaque gauche
                dernier_tir = flotte_escomptee["tirs"][-2]
                indice_der = outils.case_ln_vers_indices(dernier_tir)
                indice_der[1] = indice_der[1] - 1
                if indice_der[1] < 0:
                    # sinon droite
                    indice_der[1] = indice_der[1] + 2
                if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                    #sinon bas
                    indice_der[1]=indice_der[1]-1
                    indice_der[0]=indice_der[0]+1
                    if indice_der[0]<0:
                        #sinon haut
                        indice_der[0]=indice_der[0]-2
                    if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                        pass
                else:
                    return outils.indices_vers_case_ln(indice_der)

            if flotte_escomptee["effets"][-3] == "touche" and flotte_escomptee["effets"][-1]!="coule":
                # attaque bas
                dernier_tir = flotte_escomptee["tirs"][-3]
                indice_der = outils.case_ln_vers_indices(dernier_tir)
                indice_der[0] = indice_der[0] + 1
                if indice_der[0] < 0:
                    # sinon haut
                    indice_der[0] = indice_der[0] - 2
                if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                    indice_der[0]=indice_der[0]-2
                    if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                        pass

                else:
                    return outils.indices_vers_case_ln(indice_der)
            if flotte_escomptee["effets"][-4] == "touche" and flotte_escomptee["effets"][-1] != "coule":
                # attaque haut
                dernier_tir = flotte_escomptee["tirs"][-4]
                indice_der = outils.case_ln_vers_indices(dernier_tir)
                indice_der[0] = indice_der[0] -1
                if indice_der[0] < 0:
                    # sinon bas
                    indice_der[0] = indice_der[0] + 2
                if outils.indices_vers_case_ln(indice_der) in flotte_escomptee["tirs"]:
                    pass
                else:
                    return outils.indices_vers_case_ln(indice_der)



        except:

            pass
        for i in range(11):
            indice0 = int(10 - i)
            if indice0 == 10:
                indice0 -= 1

            for y in range(8):
                if indice0 % 2 == 1:
                    indice1 = int(9 - (y + y % 2)) - 1
                if indice0 % 2 == 0:
                    indice1 = int(9 - (y + y % 2))
                indice_final.insert(0, indice0)
                indice_final.insert(1, indice1)
                # print(indice0)
                indice_final = outils.indices_vers_case_ln(indice_final)
                if indice_final not in flotte_escomptee["tirs"]:
                    return indice_final
                else:
                    indice_final = []

            if indice_final == []:
                #print("Debug tir aléatoire")
                return flottes.tir_aleatoire(flotte_escomptee)

    return tir



def main():
    pass


if __name__ == '__main__':
    main()
