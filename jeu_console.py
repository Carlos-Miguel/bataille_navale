#! /usr/bin/env python3
# coding: UTF-8
"""
Script: outil.py
"""
import flottes
import terminal
import reseau
import outils

# Fonctions à développer selon le cahier des charges

def bataille_manuel(ma_flotte, sa_flotte, joueur):
    resultats = ['gagne', 'perd', 'abandonne']

    commandes = ['stop', 'sauv']

    j1 = ma_flotte
    j2 = sa_flotte
    if joueur == 'moi':
        i = 0
    else:
        i = 1
    resultat = False
    while not resultat:
        if i % 2 == 0:
            print("C'est au tour du joueur 1")
            saisie_joueur = terminal.saisie_cible_valide_ou_commande(commandes)
            if saisie_joueur == 'stop':
                return resultats[2]
            elif saisie_joueur == 'sauv':
                flottes.sauvegarde_partie(j1, j2)
            else:
                flottes.analyse_tir(j2, saisie_joueur)
            #terminal.affiche_flotte_2d(j1)
            #terminal.affiche_flotte_2d(j2, True)
            if flottes.est_flotte_coulee(j2):
                return resultats[0]
        else:
            print("C'est au tour du joueur 2")
            saisie_joueur = terminal.saisie_cible_valide_ou_commande(commandes)
            if saisie_joueur == 'stop':
                return resultats[2]
            elif saisie_joueur == 'sauv':
                flottes.sauvegarde_partie(j1, j2)
            else:
                flottes.analyse_tir(j1, saisie_joueur)
            #terminal.affiche_flotte_2d(j2)
            #terminal.affiche_flotte_2d(j1, True)
            if flottes.est_flotte_coulee(j1):
                return resultats[1]
        i += 1


def bataille_reseau_tir_recu(ma_flotte, sa_flotte, case, socket_actif, no_interation):
    effet = flottes.analyse_tir(ma_flotte, case)
    reponse = '[resultat]' + case
    if effet == "touche":
        reponse += "|touche"
        reseau.repond_au_serveur(socket_actif, reponse)
    if effet == "eau":
        reponse += "|eau"
        reseau.repond_au_serveur(socket_actif, reponse)
    if effet == "coule":
        reponse += "|coule"
        reseau.repond_au_serveur(socket_actif, reponse)
    if effet == "gagne":
        reponse += "|gagne"
        reseau.repond_au_serveur(socket_actif, reponse)
    return effet


def bataille_reseau(ma_flotte, ip, port):
    sa_flotte = flottes.initialisation_flotte_vide() # initialise sa flotte vite
    no_iteration = ""
    resultats = ['gagne', 'perd', 'abandonne']

    socket_actif = reseau.connexion_serveur(ip, port)
    try:
        recep = reseau.recuperation_commande_serveur(socket_actif)
    except:
       return
    pseudo = terminal.saisie_pseudo()
    reseau.repond_au_serveur(socket_actif, recep + pseudo)

    commande = reseau.parseur_commande(reseau.recuperation_commande_serveur(socket_actif))

    oui = False
    while not oui:
        if commande[0] == "[attente]" or commande[0] =="[start]":
            reseau.repond_au_serveur(socket_actif, "[ack]")
        if commande[0] == '[cible]':
            resultat_touche = bataille_reseau_envoyer_tir(ma_flotte, sa_flotte, socket_actif, no_iteration)
            if resultat_touche in resultats:
                return resultat_touche
            if resultat_touche=="stop":
                return "abandonne"

        if commande[0] == '[tir]':
            resultat_touche = bataille_reseau_tir_recu(ma_flotte, sa_flotte, commande[1], socket_actif, no_iteration)
            if resultat_touche in resultats:
                return "perd"
            commande = reseau.parseur_commande(reseau.recuperation_commande_serveur(socket_actif))

        else:
            commande = reseau.parseur_commande(reseau.recuperation_commande_serveur(socket_actif)) # tu recupere une nouvelle commande
            if commande[0] == "[fin]":
                reseau.deconnexion_serveur(socket_actif)
                return "abandonne"


def bataille_reseau_envoyer_tir(ma_flotte, sa_flotte, socket_actif, no_iteration):
    reponse = terminal.saisie_cible_valide_ou_commande("commandes")
    if reponse in ["sauv", "stop"]:
        reseau.repond_au_serveur(socket_actif, "[fin]")
        return "stop"
    else:
        reponse_final = '[tir]' + reponse
        reseau.repond_au_serveur(socket_actif, reponse_final)
        test = reseau.recuperation_commande_serveur(socket_actif)
        # print("1",test)
        reseau.repond_au_serveur(socket_actif, "[ack]")
        test = reseau.parseur_commande(test)
        # print("2",test)
        # print("3",test[2])
        try:
            flottes.memorise_action_tir_sur_flotte_inconnue(sa_flotte, reponse, test[2])
            return test[2]
        except IndexError:
            return
        #    print(test) #J'ai fait ça juste pour débug la fonction bataille reseau

def bataille_auto(ma_flotte, sa_flotte, joueur, mode_jeu):

    resultats = ['gagne', 'perd', 'abandonne']

    commandes = ['stop', 'sauv']

    j1 = ma_flotte
    j2 = sa_flotte
    if joueur == 'moi':
        i = -1
    else:
        i = 0
    resultat = False
    while not resultat:
        if mode_jeu == 'auto':
            i += 1
            if i % 2 == 0:
                print("C'est au tour du joueur 1")
                saisie_joueur = terminal.saisie_cible_valide_ou_commande(commandes)
                if saisie_joueur == 'stop':
                    return resultats[2]
                elif saisie_joueur == 'sauv':
                    flottes.sauvegarde_partie(j1, j2)
                else:
                    flottes.analyse_tir(j2, saisie_joueur)

                #terminal.affiche_flotte_2d_couleurs(j1)
                #terminal.affiche_flotte_2d_couleurs(j2, True)
                if flottes.est_flotte_coulee(j2):
                    return resultats[0]
            else:
                print("C'est au tour de l'adversaire")
                saisie_joueur = flottes.tir_aleatoire(j1)
                flottes.analyse_tir(j1, saisie_joueur)
                #terminal.affiche_flotte_2d_couleurs(j2)
                #terminal.affiche_flotte_2d_couleurs(j1, True)
                if flottes.est_flotte_coulee(j1):
                    return resultats[1]

def initialisation_bataille_reseau(ma_flotte, sa_flotte, ip, port):
    socket_actif = reseau.connexion_serveur(ip, port)
    if socket_actif:
        commande = "pas_start"
        while commande[0] not in ['[start]', '[fin]', '[timeout]']:        # commande[0] = commande du serveur
            commande = reseau.parseur_commande(reseau.recuperation_commande_serveur(socket_actif))
            if commande[0] == '[pseudo]':
                mon_pseudo = terminal.saisie_pseudo()
                ma_flotte['pseudo'] = mon_pseudo
                reseau.repond_commande_au_serveur(socket_actif, commande[0], mon_pseudo)
            elif commande[0] == '[attente]':
                reseau.repond_commande_au_serveur(socket_actif, commande[0])
            elif commande[0] == '[start]':
                sa_flotte['pseudo'] = commande[1]        # Commande[1] = argument, dans notre cas le pseudo de l'adversaire
                reseau.repond_commande_au_serveur(socket_actif, commande[0])
            elif commande[0] in ['[fin]', '[timeout]']:
                reseau.deconnexion_serveur(socket_actif)
                return None
        return socket_actif

def choix_flotte():
    fini = False
    while not fini:
        choix = terminal.saisie_mode_initialisation_flottes()
        if choix == 'endur':
            ma_flotte = flottes.initialisation_flotte_par_dictionnaire_fixe('flotte1')
            sa_flotte = flottes.initialisation_flotte_par_dictionnaire_fixe('flotte2')
            fini = True
        elif choix == 'manuel':
            ma_flotte = flottes.initialisation_flotte_vide()
            terminal.choix_flotte_manuel_console(ma_flotte)
            sa_flotte = flottes.initialisation_flotte_vide()
            fini = True
        elif choix == 'manuel+aleatoire':
            ma_flotte = flottes.initialisation_flotte_vide()
            terminal.choix_flotte_manuel_console(ma_flotte)
            sa_flotte = flottes.initialisation_flotte_vide()
            flottes.choix_flotte_aleatoire(sa_flotte)
            fini = True
        elif choix == 'aleatoires':
            ma_flotte = flottes.initialisation_flotte_vide()
            flottes.choix_flotte_aleatoire(ma_flotte)
            sa_flotte = flottes.initialisation_flotte_vide()
            flottes.choix_flotte_aleatoire(sa_flotte)
            fini = True
        elif choix == 'restaurees':
            ma_flotte = flottes.initialisation_flotte_vide()
            sa_flotte = flottes.initialisation_flotte_vide()
            if not flottes.restauration_partie(ma_flotte, sa_flotte):
                fini = False
            else:
                fini = True
    return [ma_flotte, sa_flotte]

def partie():
    flottes = choix_flotte()
    ma_flotte = flottes[0]
    sa_flotte = flottes[1]
    joueur = outils.joueur_aleatoire()
    stop = False
    while not stop:
        choix_jeu = terminal.saisie_mode_choix_jeu()
        if choix_jeu == "manuel":
            mon_pseudo = terminal.saisie_pseudo()
            res = bataille_manuel(ma_flotte, sa_flotte, joueur)
            outils.ajoute_statistiques_joueur(mon_pseudo, res)
            stop = True
        elif choix_jeu == "auto":
            mon_pseudo = terminal.saisie_pseudo()
            res = bataille_auto(ma_flotte, sa_flotte, joueur, "auto")
            outils.ajoute_statistiques_joueur(mon_pseudo, res)
            stop = True
        elif choix_jeu == "ia":
            mon_pseudo = terminal.saisie_pseudo()
            res = bataille_auto(ma_flotte, sa_flotte, joueur, "ia")
            outils.ajoute_statistiques_joueur(mon_pseudo, res)
            stop = False
        elif choix_jeu == "reseau":
            server = terminal.saisie_adresse_reseau()
            res = bataille_reseau(ma_flotte, server[0], server[1])
            outils.ajoute_statistiques_joueur(ma_flotte['pseudo'], res)
            stop = True
        outils.affichage_statistiques_joueurs()
    return




# Programme principal pour tester vos fonctions
def main():
    partie();

if __name__ == '__main__':
    main()
